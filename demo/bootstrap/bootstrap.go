package bootstrap

import (
	"context"
	"dwalker/demo/bot"
	"dwalker/demo/clients"
	"dwalker/demo/config"
	"dwalker/demo/services"
	"dwalker/demo/utils"
	"github.com/rs/zerolog/log"
	"go.uber.org/fx"
)

var Module = fx.Options(
	fx.Invoke(utils.InitLogger),
	config.Module,
	clients.Module,
	services.Module,
	bot.Module,
	fx.Invoke(bootstrap),
)

func bootstrap(
	lifecycle fx.Lifecycle,
	bot *bot.Bot,
) {
	lifecycle.Append(fx.Hook{
		OnStart: func(context.Context) error {
			log.Info().Msg("Starting service")
			go func() {
				bot.Run()
			}()
			return nil
		},
		OnStop: func(context.Context) error {
			log.Info().Msg("Stopping service")
			return nil
		},
	})
}
