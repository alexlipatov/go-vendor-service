# \SubjectApi

All URIs are relative to *https://localhost:8080/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ArticlesArticleIdSubjectsGet**](SubjectApi.md#ArticlesArticleIdSubjectsGet) | **Get** /articles/{articleId}/subjects | Get list of subjects
[**ArticlesArticleIdSubjectsPost**](SubjectApi.md#ArticlesArticleIdSubjectsPost) | **Post** /articles/{articleId}/subjects | Add Subject
[**ArticlesArticleIdSubjectsSubjectIdGet**](SubjectApi.md#ArticlesArticleIdSubjectsSubjectIdGet) | **Get** /articles/{articleId}/subjects/{subjectId} | Get Subject
[**ArticlesArticleIdSubjectsSubjectIdPost**](SubjectApi.md#ArticlesArticleIdSubjectsSubjectIdPost) | **Post** /articles/{articleId}/subjects/{subjectId} | update Subject


# **ArticlesArticleIdSubjectsGet**
> []SubjectsSubject ArticlesArticleIdSubjectsGet(ctx, articleId, options)
Get list of subjects

get subjects with options

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **articleId** | **string**| article ID | 
  **options** | [**SubjectsListOptions**](SubjectsListOptions.md)| List options | 

### Return type

[**[]SubjectsSubject**](subjects.Subject.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ArticlesArticleIdSubjectsPost**
> ArticlesArticleIdSubjectsPost(ctx, articleId, subject)
Add Subject

add subject

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **articleId** | **string**| article ID | 
  **subject** | [**SubjectsSubjectDto**](SubjectsSubjectDto.md)| New subject content | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ArticlesArticleIdSubjectsSubjectIdGet**
> SubjectsSubject ArticlesArticleIdSubjectsSubjectIdGet(ctx, articleId, subjectId)
Get Subject

get subject by uuid

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **articleId** | **string**| article ID | 
  **subjectId** | **string**| subject ID | 

### Return type

[**SubjectsSubject**](subjects.Subject.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ArticlesArticleIdSubjectsSubjectIdPost**
> ArticlesArticleIdSubjectsSubjectIdPost(ctx, updateDto, articleId, subjectId)
update Subject

update subject

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **updateDto** | [**SubjectsSubjectUpdateDto**](SubjectsSubjectUpdateDto.md)| update subject | 
  **articleId** | **string**| article ID | 
  **subjectId** | **string**| subject ID | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

