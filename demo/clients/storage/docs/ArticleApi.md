# \ArticleApi

All URIs are relative to *https://localhost:8080/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ArticlesArticleIdGet**](ArticleApi.md#ArticlesArticleIdGet) | **Get** /articles/{articleId} | Get Article
[**ArticlesArticleIdPost**](ArticleApi.md#ArticlesArticleIdPost) | **Post** /articles/{articleId} | Update Article
[**ArticlesGet**](ArticleApi.md#ArticlesGet) | **Get** /articles | Get Article
[**ArticlesPost**](ArticleApi.md#ArticlesPost) | **Post** /articles | Add Article


# **ArticlesArticleIdGet**
> CommonArticle ArticlesArticleIdGet(ctx, articleId)
Get Article

get article by uuid

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **articleId** | **string**| article ID | 

### Return type

[**CommonArticle**](common.Article.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ArticlesArticleIdPost**
> ArticlesArticleIdPost(ctx, articleId, vendor)
Update Article

update article

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **articleId** | **string**| article ID | 
  **vendor** | [**ArticlesArticleUpdateDto**](ArticlesArticleUpdateDto.md)| New article content | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ArticlesGet**
> []CommonArticle ArticlesGet(ctx, options)
Get Article

get article with options

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **options** | [**ArticlesListOptions**](ArticlesListOptions.md)| list article options | 

### Return type

[**[]CommonArticle**](common.Article.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ArticlesPost**
> ArticlesPost(ctx, article)
Add Article

add article

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **article** | [**ArticlesArticleDto**](ArticlesArticleDto.md)| New Article content | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

