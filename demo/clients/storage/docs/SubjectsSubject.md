# SubjectsSubject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Amount** | **int32** |  | [optional] [default to null]
**Article** | **string** |  | [optional] [default to null]
**ClosedAt** | **string** |  | [optional] [default to null]
**CreatedAt** | **string** |  | [optional] [default to null]
**Position** | **string** |  | [optional] [default to null]
**Status** | **string** |  | [optional] [default to null]
**Uuid** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


