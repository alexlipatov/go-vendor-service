/*
 * Article Storage
 *
 * Microservice for basic methods on articles storage
 *
 * API version: 1.0
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package storage

type ArticlesListOptions struct {
	Limit    int32  `json:"limit,omitempty"`
	NamePart string `json:"name_part,omitempty"`
}
