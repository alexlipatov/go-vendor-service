/*
 * Article Storage
 *
 * Microservice for basic methods on articles storage
 *
 * API version: 1.0
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package storage

type ArticlesArticleUpdateDto struct {
	Description string `json:"description,omitempty"`
	Name        string `json:"name,omitempty"`
}
