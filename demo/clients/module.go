package clients

import (
	"dwalker/demo/clients/storage"
	"dwalker/demo/config"
	"go.uber.org/fx"
)

var Module = fx.Options(fx.Provide(NewStorageClient))

func NewStorageClient(cfg config.Config) *storage.APIClient {
	apiConfig := storage.NewConfiguration()
	apiConfig.Scheme = "http"
	apiConfig.BasePath = cfg.Storage.BaseURL

	return storage.NewAPIClient(apiConfig)
}
