package bot

import (
	"context"
	"dwalker/demo/config"
	"dwalker/demo/services"
	"fmt"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	tg "gopkg.in/tucnak/telebot.v2"
	"math/rand"
	"strings"
	"time"
)

const (
	articlesCount = 64
	subjectsCount = 128
)

type Bot struct {
	client   *tg.Bot
	creator  *services.Creator
	receiver *services.Receiver
}

// MakeBot initialize client by config and assign handlers for bot
func MakeBot(cfg config.Config, creator *services.Creator, receiver *services.Receiver) (*Bot, error) {
	client, err := tg.NewBot(tg.Settings{
		Token:  cfg.Token,
		Poller: &tg.LongPoller{Timeout: 10 * time.Second},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to init client")
	}

	bot := Bot{
		client:   client,
		creator:  creator,
		receiver: receiver,
	}

	err = bot.assignHandlers()
	if err != nil {
		return nil, errors.Wrap(err, "failed to assign handlers")
	}

	return &bot, nil
}

// Run begins polling updates
func (b *Bot) Run() {
	b.client.Start()
}

func (b *Bot) assignHandlers() error {
	b.client.Handle("/check", b.healthCheck)
	b.client.Handle("/new_articles", b.newArticles)
	b.client.Handle("/new_subjects", b.newSubjects)
	b.client.Handle("/get_articles", b.getArticles)
	b.client.Handle("/get_subjects", b.getSubjects)

	return nil
}

func (b *Bot) healthCheck(m *tg.Message) {
	_, err := b.client.Send(m.Sender, "I am alive!")
	if err != nil {
		log.Err(err).Msg("failed to send health check message")
		return
	}
}

func (b *Bot) newArticles(m *tg.Message) {
	err := b.creator.CreateArticles(context.Background(), articlesCount)
	if err != nil {
		log.Err(err).Msg("failed to create articles")
		_ = b.replyFailure(m)
		return
	}
	_, err = b.client.Send(m.Sender, fmt.Sprintf("%d articles created", articlesCount))
	if err != nil {
		log.Err(err).Msg("failed to send message")
		_ = b.replyFailure(m)
	}
}

func (b *Bot) newSubjects(m *tg.Message) {
	articles, err := b.receiver.Articles(context.Background(), articlesCount)
	if err != nil {
		log.Err(err).Msg("failed to receive articles")
		_ = b.replyFailure(m)
	}
	randomArticleInd := rand.Intn(len(articles))

	articleId, err := uuid.Parse(articles[randomArticleInd].Uuid)
	if err != nil {
		log.Err(err).Msg("failed to parse uuid, unexpected error")
		_ = b.replyFailure(m)
		return
	}

	err = b.creator.CreateSubjects(context.Background(), articleId, subjectsCount)
	if err != nil {
		log.Err(err).Msg("failed to create subjects")
		_ = b.replyFailure(m)
		return
	}

	_, err = b.client.Send(m.Sender, fmt.Sprintf("%d subjects created (%s)", subjectsCount, articles[randomArticleInd].Name))
	if err != nil {
		log.Err(err).Msg("failed to send message")
		_ = b.replyFailure(m)
	}
}

func (b *Bot) getArticles(m *tg.Message) {
	articles, err := b.receiver.Articles(context.Background(), articlesCount)
	if err != nil {
		log.Err(err).Msg("failed to receive articles")
		return
	}

	var messageContent = make([]string, len(articles))
	for i, article := range articles {
		messageContent[i] = article.Name
	}
	content := strings.Join(messageContent, "\n")
	if content == "" {
		content = "no articles"
	}
	if len(content) > 4096 {
		content = content[:4090] + "\n..."
	}

	_, err = b.client.Send(m.Sender, content)
	if err != nil {
		log.Err(err).Msg("failed to send articles")
		_ = b.replyFailure(m)
	}
}

func (b *Bot) getSubjects(m *tg.Message) {
	articles, err := b.receiver.Articles(context.Background(), articlesCount)
	if err != nil {
		log.Err(err).Msg("failed to receive articles")
		return
	}

	var messageContent = make([]string, 0)
	for _, article := range articles {
		subjects, err := b.receiver.Subjects(context.Background(), uuid.MustParse(article.Uuid), subjectsCount)
		if err != nil {
			log.Err(err).Msg("failed to receive subjects")
		}
		for _, subject := range subjects {
			subjectView := fmt.Sprintf("%s\t%s\t%d", article.Name, subject.Article[:8], subject.Amount)
			messageContent = append(messageContent, subjectView)
		}
	}
	content := strings.Join(messageContent, "\n")
	if content == "" {
		content = "no subjects"
	}
	if len(content) > 4096 {
		content = content[:4090] + "\n..."
	}

	_, err = b.client.Send(m.Sender, content)
	if err != nil {
		log.Err(err).Msg("failed to send articles")
		_ = b.replyFailure(m)
	}
}

func (b *Bot) replyFailure(m *tg.Message) error {
	_, err := b.client.Send(m.Sender, "failed to execute command")
	return err
}
