package services

import (
	"dwalker/demo/clients/storage"
	"fmt"
	"github.com/google/uuid"
	"strings"
)
import "math/rand"

var articleNames = []string{
	"Chairs with 2 legs",
	"Broken Tables",
	"Heavy Helmets",
	"Golden Doors",
	"Glasses",
	"Fake Computers",
}

const defaultDescription = "default description here!"

// newArticleDto returns unique dto
func newArticleDto() storage.ArticlesArticleDto {
	name := articleNames[rand.Intn(len(articleNames))]
	uniqueName := strings.Join([]string{name, uuid.NewString()[:8]}, " ")

	return storage.ArticlesArticleDto{
		Description: defaultDescription,
		Name:        uniqueName,
	}
}

func newSubjectDto() storage.SubjectsSubjectDto {
	amount := (rand.Intn(500) % 10) + 10

	position := fmt.Sprintf("%d;%d;%d", rand.Intn(32), rand.Intn(32), rand.Intn(32))

	return storage.SubjectsSubjectDto{
		Amount:   int32(amount),
		Position: position,
		Status:   "proposed",
	}
}
