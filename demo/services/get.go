package services

import (
	"context"
	"dwalker/demo/clients/storage"
	"github.com/google/uuid"
	"net/http"

	"github.com/pkg/errors"
)

func NewReceiver(client *storage.APIClient) *Receiver {
	return &Receiver{client: client}
}

type Receiver struct {
	client *storage.APIClient
}

func (r Receiver) Articles(ctx context.Context, n int) ([]storage.CommonArticle, error) {
	options := storage.ArticlesListOptions{
		Limit:    int32(n),
		NamePart: "",
	}

	result, response, err := r.client.ArticleApi.ArticlesGet(ctx, options)
	if err != nil || response.StatusCode != http.StatusOK {
		return nil, errors.Wrap(err, "failed to get articles")
	}

	return result, nil
}

func (r Receiver) Subjects(ctx context.Context, articleId uuid.UUID, n int) ([]storage.SubjectsSubject, error) {
	options := storage.SubjectsListOptions{Limit: int32(n)}

	result, response, err := r.client.SubjectApi.ArticlesArticleIdSubjectsGet(ctx, articleId.String(), options)
	if err != nil || response.StatusCode != http.StatusOK {
		return nil, errors.Wrap(err, "failed to get articles")
	}

	return result, nil
}
