package services

import (
	"context"
	"dwalker/demo/clients/storage"
	"github.com/google/uuid"
	"net/http"

	"github.com/pkg/errors"
)

func NewCreator(client *storage.APIClient) *Creator {
	return &Creator{client: client}
}

type Creator struct {
	client *storage.APIClient
}

func (c Creator) CreateArticles(ctx context.Context, n int) error {
	for i := 0; i < n; i++ {
		response, err := c.client.ArticleApi.ArticlesPost(ctx, newArticleDto())
		if err != nil || response.StatusCode != http.StatusCreated {
			return errors.Wrap(err, "failed to post new articles")
		}
	}
	return nil
}

func (c Creator) CreateSubjects(ctx context.Context, articleId uuid.UUID, n int) error {
	for i := 0; i < n; i++ {
		subject := newSubjectDto()
		response, err := c.client.SubjectApi.ArticlesArticleIdSubjectsPost(ctx, articleId.String(), subject)
		if err != nil || response.StatusCode != http.StatusCreated {
			return nil
		}
	}
	return nil
}
