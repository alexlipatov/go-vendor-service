package config

type Config struct {
	Token   string
	Storage struct {
		BaseURL string
	}
}
