@startuml houseman

namespace domain {
    namespace common {
        class Person {
            +ID UUID
            +Name string
            +Access []auth.Access
        }
    }

    namespace auth {
        class Access {
            Object string
            Actions []Action
        }

        Access "1" o-- "..." Action

        Enum Action {
            Edit
            View
            Delete
        }
    }
}

namespace repository {
    interface IRepository {
        +GetAccessList(ctx context.Context, person common.Person) ([]auth.Access, error)
    }

    IRepository --> domain.common.person
    IRepository --> domain.auth.Access

    namespace mongo {
        class MongoConnectionFactory {

        }

        class repository implements repository.IRepository {

        }
    }
}

namespace services {
    namespace openapi {
        class Endpoints {
            +Bind(routes gin.IRoutes)
        }
    }

    namespace auth {
        class Endpoints {
            +Bind(routes gin.IRoutes)
            +GetAccess(ctx *gin.Context)
            +Auth(ctx *gin.Context)
        }

        interface IService {
            +GetAccess(ctx context.Context, person common.Person) ([]auth.Access, error)
            +Auth(ctx context.Context, person common.Person, accesses []auth.Access) ([]AccessResponse, error)
        }

        class service implements IService {
            -repository repository.IRepository
        }

        service "1" o-- "1" repository.IRepository
    }
}

main --> repository.mongo.MongoConnectionFactory
main --> services.openapi.Endpoints
main --> services.auth.Endpoints

class main

@enduml