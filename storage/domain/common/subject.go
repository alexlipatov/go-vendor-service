package common

import (
	"encoding/json"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"time"
)

type Subject struct {
	UUID      uuid.UUID  `json:"uuid"`
	Article   uuid.UUID  `json:"article"`
	Position  string     `json:"position"`
	Amount    int        `json:"amount"`
	Status    Status     `json:"status"`
	CreatedAt time.Time  `json:"created_at"`
	ClosedAt  *time.Time `json:"closed_at"`
}

func NewSubject(uuid uuid.UUID, article uuid.UUID, amount int, position string, status Status, createdAt time.Time, closedAt *time.Time) *Subject {
	if status.IsZero() {
		status = Proposed
	}
	return &Subject{
		UUID:      uuid,
		Article:   article,
		Position:  position,
		Amount:    amount,
		Status:    status,
		CreatedAt: createdAt,
		ClosedAt:  closedAt,
	}
}

var (
	Active   = Status{"active"}
	Closed   = Status{"closed"}
	Proposed = Status{"proposed"}
)

var itemStatusValues = []Status{
	Active,
	Closed,
	Proposed,
}

type Status struct {
	a string
}

func (s Status) MarshalJSON() ([]byte, error) {
	return json.Marshal(s.a)
}

func SubjectStatusFromString(s string) (Status, error) {
	for _, status := range itemStatusValues {
		if status.String() == s {
			return status, nil
		}
	}
	return Status{}, errors.Errorf("unknown '%s' availability", s)
}

func (s Status) IsZero() bool {
	return s == Status{}
}

func (s Status) String() string {
	return s.a
}

func (i *Subject) MakeClose() {
	i.Status = Closed
	closeTime := time.Now().UTC()
	i.ClosedAt = &closeTime
}

func (i *Subject) MakeProposed() {
	i.Status = Proposed
}

func (i *Subject) MakeActive() {
	if i.Status != Closed {
		i.Status = Active
		i.ClosedAt = nil
	}
}

// SetStatus устанавливает статус предмету
func (i *Subject) SetStatus(status Status) error {
	switch status {
	case Closed:
		i.MakeClose()
	case Proposed:
		i.MakeProposed()
	case Active:
		i.MakeActive()
	default:
		return errors.New("unknown item status")
	}
	return nil
}
