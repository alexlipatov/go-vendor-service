package common

import (
	"github.com/google/uuid"
)

type Article struct {
	UUID        uuid.UUID `json:"uuid"`
	Name        string    `json:"name"`
	Description string    `json:"description"`
}

func NewArticle(uuid uuid.UUID, name string, description string) *Article {
	return &Article{
		UUID:        uuid,
		Name:        name,
		Description: description,
	}
}

func (v Article) String() string {
	return v.Name
}
