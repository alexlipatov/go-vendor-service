package main

import (
	"dwalker/storage/bootstrap"
	"github.com/rs/zerolog/log"

	"go.uber.org/fx"
)

// @title        Article Storage
// @version      1.0
// @description  Microservice for basic methods on articles storage
// @host         localhost:8080
// @BasePath     /api
// @schemes		 http
func main() {
	defer func() {
		if err := recover(); err != nil {
			log.Error().Msgf("service failed: %s", err)
		}
	}()

	fx.New(bootstrap.Module).Run()
}
