package articles

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// MakeEndpoints returns endpoints for article service
func MakeEndpoints(service IService) *Endpoints {
	return &Endpoints{service: service}
}

type Endpoints struct {
	service IService
}

func (e Endpoints) Bind(router gin.IRouter) {
	router.GET("articles/:articleId", e.GetArticle)
	router.POST("articles/:articleId", e.UpdateArticle)
	router.GET("articles", e.ListArticles)
	router.POST("articles", e.AddArticle)
}

// GetArticle
// @Summary      Get Article
// @Description  get article by uuid
// @Tags         article
// @Produce      json
// @Param        articleId  path      string  true  "article ID"
// @Success      200        {object}  common.Article
// @Failure      400
// @Failure      404
// @Router       /articles/{articleId} [get]
func (e Endpoints) GetArticle(ctx *gin.Context) {
	articleId, err := uuid.Parse(ctx.Param("articleId"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}

	article, err := e.service.GetById(ctx, articleId)
	if err != nil {
		_ = ctx.AbortWithError(http.StatusNotFound, err)
	}
	ctx.JSON(http.StatusOK, article)
}

// ListArticles
// @Summary      Get Article
// @Description  get article with options
// @Tags         article
// @Accept       json
// @Produce      json
// @Param        options  body     ListOptions  true  "list article options"
// @Success      200      {array}  common.Article
// @Failure      400
// @Router       /articles [get]
func (e Endpoints) ListArticles(ctx *gin.Context) {
	var listOptions ListOptions
	err := ctx.BindJSON(&listOptions)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
	articles, err := e.service.List(ctx, listOptions.NamePart, listOptions.Limit)
	if err != nil {
		_ = ctx.AbortWithError(http.StatusInternalServerError, err)
	}

	ctx.JSON(http.StatusOK, articles)
}

// AddArticle
// @Summary      Add Article
// @Description  add article
// @Tags         article
// @Accept       json
// @Param        article  body  ArticleDto  true  "New Article content"
// @Success      201
// @Failure      400
// @Router       /articles [post]
func (e Endpoints) AddArticle(ctx *gin.Context) {
	var article ArticleDto
	err := ctx.BindJSON(&article)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}

	_, err = e.service.Add(ctx, article)
	if err != nil {
		_ = ctx.AbortWithError(http.StatusInternalServerError, err)
	}

	ctx.Status(http.StatusCreated)
}

// UpdateArticle
// @Summary      Update Article
// @Description  update article
// @Tags         article
// @Accept       json
// @Param        articleId  path  string            true  "article ID"
// @Param        vendor     body  ArticleUpdateDto  true  "New article content"
// @Success      200
// @Failure      400
// @Router       /articles/{articleId} [post]
func (e Endpoints) UpdateArticle(ctx *gin.Context) {
	var updateDto ArticleUpdateDto
	err := ctx.BindJSON(&updateDto)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}

	articleId, err := uuid.Parse(ctx.Param("articleId"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
	err = e.service.Update(ctx, articleId, updateDto)
	if err != nil {
		_ = ctx.AbortWithError(http.StatusInternalServerError, err)
	}

	ctx.Status(http.StatusAccepted)
}
