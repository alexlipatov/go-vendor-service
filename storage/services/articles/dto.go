package articles

type ArticleUpdateDto struct {
	Name        *string `json:"name"`
	Description *string `json:"description"`
}

type ArticleDto struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}

type ListOptions struct {
	NamePart string `json:"name_part"`
	Limit    int    `json:"limit"`
}
