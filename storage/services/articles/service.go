package articles

import (
	"context"
	"dwalker/storage/domain/common"
	"dwalker/storage/repository"
	"github.com/google/uuid"
)

type IService interface {
	Add(ctx context.Context, article ArticleDto) (*common.Article, error)
	GetById(ctx context.Context, uuid uuid.UUID) (*common.Article, error)
	List(ctx context.Context, namePart string, limit int) ([]*common.Article, error)
	Update(ctx context.Context, uuid uuid.UUID, articleUpdateDto ArticleUpdateDto) error
}

// NewService returns article service
func NewService(articleRepo repository.IArticleRepository) IService {
	return &service{articleRepo: articleRepo}
}

type service struct {
	articleRepo repository.IArticleRepository
}

func (s service) Add(ctx context.Context, article ArticleDto) (*common.Article, error) {
	newArticle := *common.NewArticle(uuid.New(), article.Name, article.Description)
	return s.articleRepo.Add(ctx, newArticle)
}

func (s service) GetById(ctx context.Context, uuid uuid.UUID) (*common.Article, error) {
	return s.articleRepo.GetById(ctx, uuid)
}

func (s service) List(ctx context.Context, namePart string, limit int) ([]*common.Article, error) {
	return s.articleRepo.List(ctx, namePart, limit)
}

func (s service) Update(ctx context.Context, uuid uuid.UUID, articleUpdateDto ArticleUpdateDto) error {
	article, err := s.GetById(ctx, uuid)
	if err != nil {
		return err
	}

	newArticle, err := buildDesired(article, articleUpdateDto)
	if err != nil {
		return err
	}
	return s.articleRepo.Update(ctx, uuid, newArticle)
}

func buildDesired(article *common.Article, dto ArticleUpdateDto) (*common.Article, error) {
	if dto.Name != nil {
		article.Name = *dto.Name
	}
	if dto.Description != nil {
		article.Description = *dto.Description
	}
	return article, nil
}
