package codes

import (
	"bytes"
	"context"
	"dwalker/storage/repository"
	"encoding/gob"
	"fmt"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"github.com/skip2/go-qrcode"
	"os"
	"time"
)

// FileLifeTime is a time after file will be removed
const FileLifeTime = 2 * time.Hour

const TmpDirPath = "temporary_codes/"

type IService interface {
	GetSubjectCode(ctx context.Context, subjectId uuid.UUID) (string, error)
}

func NewService(subjectRepo repository.ISubjectRepository) IService {
	return &service{subjectRepo: subjectRepo}
}

type service struct {
	subjectRepo repository.ISubjectRepository
}

func (s service) GetSubjectCode(ctx context.Context, subjectId uuid.UUID) (string, error) {
	subject, err := s.subjectRepo.GetById(ctx, subjectId)
	if err != nil {
		return "", errors.Wrap(err, "unable to get subject by id")
	}

	err = os.Mkdir(TmpDirPath, 700)
	if err != nil {
		return "", errors.Wrap(err, "failed to create tmp dir")
	}

	var content = SubjectCode{
		ID:      subject.UUID,
		Article: subject.Article,
	}

	var buffer bytes.Buffer
	encoder := gob.NewEncoder(&buffer)
	err = encoder.Encode(content)
	if err != nil {
		return "", errors.Wrap(err, "failed to encode code content")
	}

	fileName := fmt.Sprintf("%s.png", content.ID)
	filePath := fmt.Sprintf("%s/%s", TmpDirPath, fileName)
	err = qrcode.WriteFile(string(buffer.Bytes()), qrcode.Low, -1, filePath)
	if err != nil {
		return "", errors.Wrap(err, "failed to encode bytes to raw image bytes")
	}

	go func() {
		time.Sleep(FileLifeTime)
		err := os.Remove(filePath)
		if err != nil {
			log.Err(err).Msg("unable to remove temporary code file")
		}
	}()

	return fileName, nil
}
