package codes

import (
	"github.com/google/uuid"
)

type SubjectCode struct {
	ID      uuid.UUID
	Article uuid.UUID
}

type SubjectCodeDto struct {
	FileName string `json:"fileName"`
}
