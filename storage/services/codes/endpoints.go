package codes

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"net/http"
	"os"
	"path"
)

func MakeEndpoints(service IService) *Endpoints {
	return &Endpoints{service: service}
}

type Endpoints struct {
	service IService
}

func (e Endpoints) Bind(router gin.IRouter) {
	router.GET("codes/subjects/:subjectId", e.GetNewSubjectCode)
	router.GET("codes/:fileName", e.AccessSubjectCode)
}

func (e Endpoints) GetNewSubjectCode(ctx *gin.Context) {
	subjectId, err := uuid.Parse(ctx.Param("subjectId"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	fileName, err := e.service.GetSubjectCode(ctx, subjectId)
	if err != nil {
		_ = ctx.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	var subjectCodeDto = SubjectCodeDto{
		FileName: fmt.Sprintf("%s", fileName),
	}
	ctx.JSON(http.StatusOK, subjectCodeDto)
}

func (e Endpoints) AccessSubjectCode(ctx *gin.Context) {
	fileName := ctx.Param("fileName")
	if fileName == "" {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "file name is empty"})
		return
	}

	filePath := path.Join(TmpDirPath, fileName)

	if _, err := os.Stat(filePath); errors.Is(err, os.ErrNotExist) {
		ctx.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return
	}

	ctx.Header("Content-Description", "File Transfer")
	ctx.Header("Content-Transfer-Encoding", "binary")
	ctx.Header("Content-Disposition", "attachment; filename="+fileName)
	ctx.Header("Content-Type", "application/octet-stream")
	ctx.File(filePath)
}
