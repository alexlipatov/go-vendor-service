package subjects

import (
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"net/http"
)

// MakeEndpoints returns endpoints for subject service
func MakeEndpoints(service IService) *Endpoints {
	return &Endpoints{service: service}
}

type Endpoints struct {
	service IService
}

func (e Endpoints) Bind(router gin.IRouter) {
	router.GET("articles/:articleId/subjects/:subjectId", e.GetSubject)
	router.POST("articles/:articleId/subjects/:subjectId", e.UpdateSubject)
	router.GET("articles/:articleId/subjects", e.ListSubjects)
	router.POST("articles/:articleId/subjects", e.AddSubject)
}

// AddSubject
// @Summary      Add Subject
// @Description  add subject
// @Tags         subject
// @Accept       json
// @Produce      json
// @Param        articleId  path  string      true  "article ID"
// @Param        subject    body  SubjectDto  true  "New subject content"
// @Success      201
// @Failure      400
// @Router       /articles/{articleId}/subjects [post]
func (e Endpoints) AddSubject(ctx *gin.Context) {
	var subject SubjectDto
	err := ctx.BindJSON(&subject)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}

	articleId, err := uuid.Parse(ctx.Param("articleId"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}

	_, err = e.service.Add(ctx, articleId, subject)
	if err != nil {
		_ = ctx.AbortWithError(http.StatusInternalServerError, err)
	}
	ctx.Status(http.StatusCreated)
}

// ListSubjects
// @Summary      Get list of subjects
// @Description  get subjects with options
// @Tags         subject
// @Accept       json
// @Produce      json
// @Param        articleId  path     string       true  "article ID"
// @Param        options    body     ListOptions  true  "List options"
// @Success      200        {array}  Subject
// @Failure      400
// @Router       /articles/{articleId}/subjects [get]
func (e Endpoints) ListSubjects(ctx *gin.Context) {
	var listOptions ListOptions
	err := ctx.BindJSON(&listOptions)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}

	articleId, err := uuid.Parse(ctx.Param("articleId"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}

	result, err := e.service.List(ctx, articleId, listOptions.Limit)
	if err != nil {
		_ = ctx.AbortWithError(http.StatusInternalServerError, err)
	}

	ctx.JSON(http.StatusOK, result)
}

// UpdateSubject
// @Summary      update Subject
// @Description  update subject
// @Tags         subject
// @Produce      json
// @Param        updateDto  body  SubjectUpdateDto  true  "update subject"
// @Param        articleId  path  string            true  "article ID"
// @Param        subjectId  path  string            true  "subject ID"
// @Success      200
// @Failure      400
// @Router       /articles/{articleId}/subjects/{subjectId} [post]
func (e Endpoints) UpdateSubject(ctx *gin.Context) {
	var updateOptions SubjectUpdateDto
	err := ctx.BindJSON(&updateOptions)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}

	subjectId, err := uuid.Parse(ctx.Param("subjectId"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}

	err = e.service.Update(ctx, subjectId, updateOptions)
	if err != nil {
		_ = ctx.AbortWithError(http.StatusInternalServerError, err)
	}
	ctx.Status(http.StatusAccepted)
}

// GetSubject
// @Summary      Get Subject
// @Description  get subject by uuid
// @Tags         subject
// @Produce      json
// @Param        articleId  path      string  true  "article ID"
// @Param        subjectId  path      string  true  "subject ID"
// @Success      200        {object}  Subject
// @Failure      400
// @Failure      404
// @Router       /articles/{articleId}/subjects/{subjectId} [get]
func (e Endpoints) GetSubject(ctx *gin.Context) {
	_, err := uuid.Parse(ctx.Param("articleId"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}

	subjectId, err := uuid.Parse(ctx.Param("subjectId"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}

	item, err := e.service.GetById(ctx, subjectId)
	if err != nil {
		_ = ctx.AbortWithError(http.StatusNotFound, err)
	}
	ctx.JSON(http.StatusOK, item)
}
