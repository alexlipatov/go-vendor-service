package subjects

import (
	"context"
	"dwalker/storage/domain/common"
	"dwalker/storage/repository"
	"github.com/google/uuid"
	"time"
)

type IService interface {
	Add(ctx context.Context, articleId uuid.UUID, subject SubjectDto) (*common.Subject, error)
	GetById(ctx context.Context, uuid uuid.UUID) (*common.Subject, error)
	List(ctx context.Context, articleId uuid.UUID, limit int) ([]*common.Subject, error)
	Update(ctx context.Context, uuid uuid.UUID, subjectUpdateDto SubjectUpdateDto) error
}

// NewService returns subject service
func NewService(subjectRepository repository.ISubjectRepository) IService {
	return &service{subjectRepo: subjectRepository}
}

type service struct {
	subjectRepo repository.ISubjectRepository
}

func (s service) Add(ctx context.Context, articleId uuid.UUID, subject SubjectDto) (*common.Subject, error) {
	status, err := common.SubjectStatusFromString(subject.Status)
	if err != nil {
		return nil, err
	}

	var newSubject = *common.NewSubject(
		uuid.New(),
		articleId,
		subject.Amount,
		subject.Position,
		status,
		time.Now(),
		nil,
	)
	return s.subjectRepo.Add(ctx, newSubject)
}

func (s service) GetById(ctx context.Context, uuid uuid.UUID) (*common.Subject, error) {
	return s.subjectRepo.GetById(ctx, uuid)
}

func (s service) List(ctx context.Context, articleId uuid.UUID, limit int) ([]*common.Subject, error) {
	return s.subjectRepo.List(ctx, articleId, limit)
}

func (s service) Update(ctx context.Context, uuid uuid.UUID, subjectUpdateDto SubjectUpdateDto) error {
	subject, err := s.GetById(ctx, uuid)
	if err != nil {
		return err
	}

	newSubject, err := buildDesired(subject, subjectUpdateDto)
	if err != nil {
		return err
	}
	return s.subjectRepo.Update(ctx, uuid, newSubject)
}

func buildDesired(subject *common.Subject, dto SubjectUpdateDto) (*common.Subject, error) {
	if dto.Position != nil {
		subject.Position = *dto.Position
	}
	if dto.Status != nil {
		status, err := common.SubjectStatusFromString(*dto.Status)
		if err != nil {
			return subject, err
		}
		err = subject.SetStatus(status)
		return subject, err
	}
	return subject, nil
}
