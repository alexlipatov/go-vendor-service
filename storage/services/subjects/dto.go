package subjects

import "time"

type SubjectUpdateDto struct {
	Position *string `json:"position,omitempty"`
	Status   *string `json:"status,omitempty"`
}

type SubjectDto struct {
	Position string `json:"position"`
	Amount   int    `json:"amount"`
	Status   string `json:"status"`
}

type ListOptions struct {
	Limit int `json:"limit"`
}

type Subject struct {
	UUID      string     `json:"uuid"`
	Article   string     `json:"article"`
	Position  string     `json:"position"`
	Amount    int        `json:"amount"`
	Status    string     `json:"status"`
	CreatedAt time.Time  `json:"created_at"`
	ClosedAt  *time.Time `json:"closed_at"`
}
