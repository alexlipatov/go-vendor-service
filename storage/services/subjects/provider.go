package subjects

import "go.uber.org/fx"

// Module provides subject service dependencies
var Module = fx.Options(fx.Provide(NewService), fx.Provide(MakeEndpoints))
