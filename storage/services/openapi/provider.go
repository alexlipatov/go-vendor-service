package openapi

import "go.uber.org/fx"

// Module provides dependencies for openapi endpoints
var Module = fx.Options(fx.Provide(MakeEndpoints))
