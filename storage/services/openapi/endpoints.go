package openapi

import (
	"github.com/gin-gonic/gin"
)

// MakeEndpoints returns bindable endpoints
func MakeEndpoints() *Endpoints {
	return &Endpoints{}
}

type Endpoints struct {
}

func (e Endpoints) Bind(router gin.IRouter) {
	router.StaticFile("swagger.json", "docs/swagger.json")
}
