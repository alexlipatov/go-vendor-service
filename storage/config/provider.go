package config

import "go.uber.org/fx"

// Module provides config dependencies
var Module = fx.Options(fx.Provide(MustLoad))
