package config

// Config is a structured application configuration values
type Config struct {
	Port     int
	Database struct {
		Address string
	}
}
