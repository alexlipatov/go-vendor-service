package bootstrap

import (
	"context"
	"dwalker/storage/config"
	"dwalker/storage/lib"
	"dwalker/storage/repository"
	"dwalker/storage/services/articles"
	"dwalker/storage/services/codes"
	"dwalker/storage/services/openapi"
	"dwalker/storage/services/subjects"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"go.uber.org/fx"
)

// Module in bootstrap provides all required application modules
var Module = fx.Options(
	fx.Invoke(lib.InitLogger),
	config.Module,
	repository.Module,
	articles.Module,
	subjects.Module,
	codes.Module,
	openapi.Module,
	fx.Provide(SetupServer),
	fx.Invoke(bootstrap),
)

func bootstrap(
	lifecycle fx.Lifecycle,
	server *gin.Engine,
	cfg config.Config,
) {
	lifecycle.Append(fx.Hook{
		OnStart: func(context.Context) error {
			log.Info().Msg("Starting service")
			go func() {
				_ = server.Run(fmt.Sprintf(":%d", cfg.Port))
			}()
			return nil
		},
		OnStop: func(context.Context) error {
			log.Info().Msg("Stopping service")
			return nil
		},
	})
}

// SetupServer setups gin engine and it's routes
func SetupServer(
	subjects *subjects.Endpoints,
	articles *articles.Endpoints,
	openapi *openapi.Endpoints,
	codes *codes.Endpoints,
) *gin.Engine {
	server := gin.Default()

	api := server.Group("api/")

	subjects.Bind(api)
	articles.Bind(api)
	codes.Bind(api)
	openapi.Bind(api)

	return server
}
