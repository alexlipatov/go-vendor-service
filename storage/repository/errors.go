package repository

import "errors"

var NotFoundErr = errors.New("not found")
