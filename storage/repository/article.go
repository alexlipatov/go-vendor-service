package repository

import (
	"context"
	"dwalker/storage/domain/common"
	"dwalker/storage/repository/ent"
	"dwalker/storage/repository/ent/article"
	"github.com/google/uuid"
	"github.com/pkg/errors"
)

// IArticleRepository to access articles
type IArticleRepository interface {
	Add(ctx context.Context, article common.Article) (*common.Article, error)
	GetById(ctx context.Context, uuid uuid.UUID) (*common.Article, error)
	List(ctx context.Context, namePart string, limit int) ([]*common.Article, error)
	Update(ctx context.Context, uuid uuid.UUID, desired *common.Article) error
}

// MakeArticleRepository returns repository to access articles
func MakeArticleRepository(client *ent.Client) IArticleRepository {
	return &articleRepo{client: client}
}

type articleRepo struct {
	client *ent.Client
}

func (a articleRepo) Add(ctx context.Context, article common.Article) (*common.Article, error) {
	articleDbo, err := a.client.Article.Create().
		SetID(article.UUID).
		SetName(article.Name).
		SetDescription(article.Description).Save(ctx)

	if err != nil {
		return nil, errors.Wrap(err, "unable to create new article")
	}
	return mapArticleDboToDomain(articleDbo), nil
}

func (a articleRepo) GetById(ctx context.Context, uuid uuid.UUID) (*common.Article, error) {
	articleDbo, err := a.client.Article.Get(ctx, uuid)
	switch {
	case errors.Is(err, &ent.NotFoundError{}):
		return nil, NotFoundErr
	case err != nil:
		return nil, errors.Wrap(err, "unable to get article by id")
	default:
		return mapArticleDboToDomain(articleDbo), nil
	}
}

func (a articleRepo) List(ctx context.Context, namePart string, limit int) ([]*common.Article, error) {
	articles, err := a.client.Article.Query().Where(article.NameContains(namePart)).Limit(limit).All(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "unable to get list of articles by name part")
	}
	var result = make([]*common.Article, len(articles))
	for i, a := range articles {
		result[i] = mapArticleDboToDomain(a)
	}
	return result, nil
}

func (a articleRepo) Update(ctx context.Context, uuid uuid.UUID, desired *common.Article) error {
	_, err := a.client.Article.Update().
		Where(article.IDEQ(uuid)).
		SetName(desired.Name).
		SetDescription(desired.Description).Save(ctx)
	if err != nil {
		return errors.Wrap(err, "unable to update article")
	}
	return nil
}

func mapArticleDboToDomain(dbo *ent.Article) *common.Article {
	return &common.Article{
		UUID:        dbo.ID,
		Name:        dbo.Name,
		Description: dbo.Description,
	}
}
