package repository

import "go.uber.org/fx"

// Module provides repository dependencies
var Module = fx.Options(
	fx.Provide(NewClient),
	fx.Provide(MakeArticleRepository),
	fx.Provide(MakeSubjectRepository),
)
