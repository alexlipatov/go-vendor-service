package repository

import (
	"context"
	"dwalker/storage/domain/common"
	"dwalker/storage/repository/ent"
	"dwalker/storage/repository/ent/subject"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

// ISubjectRepository to access subjects
type ISubjectRepository interface {
	Add(ctx context.Context, subject common.Subject) (*common.Subject, error)
	GetById(ctx context.Context, uuid uuid.UUID) (*common.Subject, error)
	List(ctx context.Context, articleId uuid.UUID, limit int) ([]*common.Subject, error)
	Update(ctx context.Context, uuid uuid.UUID, desired *common.Subject) error
}

// MakeSubjectRepository returns repository to access subjects
func MakeSubjectRepository(client *ent.Client) ISubjectRepository {
	return &subjectRepository{client: client}
}

type subjectRepository struct {
	client *ent.Client
}

func (s subjectRepository) Add(ctx context.Context, subject common.Subject) (*common.Subject, error) {
	subjectDbo, err := s.client.Subject.Create().
		SetID(subject.UUID).
		SetPosition(subject.Position).
		SetAmount(subject.Amount).
		SetArticleID(subject.Article).
		SetNillableClosedAt(subject.ClosedAt).
		SetCreatedAt(subject.CreatedAt).
		SetStatus(mapDomainStatus(subject.Status)).Save(ctx)

	if err != nil {
		return nil, errors.Wrap(err, "unable to create new subject")
	}
	return mapSubjectDboToDomain(subjectDbo), nil
}

func (s subjectRepository) GetById(ctx context.Context, uuid uuid.UUID) (*common.Subject, error) {
	subjectDbo, err := s.client.Subject.Get(ctx, uuid)
	switch {
	case errors.Is(err, &ent.NotFoundError{}):
		return nil, NotFoundErr
	case err != nil:
		return nil, errors.Wrap(err, "unable to get subject by id")
	default:
		return mapSubjectDboToDomain(subjectDbo), nil
	}
}

func (s subjectRepository) List(ctx context.Context, articleId uuid.UUID, limit int) ([]*common.Subject, error) {
	subjects, err := s.client.Subject.Query().Where(subject.ArticleID(articleId)).Limit(limit).All(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "unable to get list of subjects by article")
	}
	var result = make([]*common.Subject, len(subjects))
	for i, a := range subjects {
		result[i] = mapSubjectDboToDomain(a)
	}
	return result, nil
}

func (s subjectRepository) Update(ctx context.Context, uuid uuid.UUID, desired *common.Subject) error {
	logrus.Debugf("update desired: %+v", *desired)
	_, err := s.client.Subject.Update().
		Where(subject.IDEQ(uuid)).
		Where(subject.ArticleID(desired.Article)).
		SetAmount(desired.Amount).
		SetPosition(desired.Position).
		SetStatus(mapDomainStatus(desired.Status)).
		SetNillableClosedAt(desired.ClosedAt).Save(ctx)
	if err != nil {
		return errors.Wrap(err, "unable to update article")
	}
	return nil
}

func mapSubjectDboToDomain(dbo *ent.Subject) *common.Subject {
	return &common.Subject{
		UUID:      dbo.ID,
		Article:   dbo.ArticleID,
		Position:  dbo.Position,
		Amount:    dbo.Amount,
		Status:    mapStatusToDomain(dbo.Status),
		CreatedAt: dbo.CreatedAt,
		ClosedAt:  dbo.ClosedAt,
	}
}

func mapStatusToDomain(status subject.Status) common.Status {
	switch status.String() {
	case common.Active.String():
		return common.Active
	case common.Proposed.String():
		return common.Proposed
	case common.Closed.String():
		return common.Closed
	default:
		return common.Proposed
	}
}

func mapDomainStatus(status common.Status) subject.Status {
	switch status.String() {
	case subject.StatusActive.String():
		return subject.StatusActive
	case subject.StatusProposed.String():
		return subject.StatusProposed
	case subject.StatusClosed.String():
		return subject.StatusClosed
	default:
		return subject.DefaultStatus
	}
}
