package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"github.com/google/uuid"
	"time"
)

// Subject holds the schema definition for the Subject entity.
type Subject struct {
	ent.Schema
}

// Fields of the Subject.
func (Subject) Fields() []ent.Field {
	return []ent.Field{
		field.UUID("id", uuid.UUID{}).Default(uuid.New),
		field.UUID("article_id", uuid.UUID{}),
		field.String("position").MaxLen(32),
		field.Int("amount").Positive().Min(1),
		field.Enum("status").Values("proposed", "closed", "active").Default("proposed"),
		field.Time("created_at").Default(time.Now),
		field.Time("closed_at").Nillable().Optional(),
	}
}

// Edges of the Subject.
func (Subject) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("article", Article.Type).Ref("subjects").Unique().Field("article_id").Required(),
	}
}
