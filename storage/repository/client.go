package repository

import (
	"context"
	"database/sql"
	"dwalker/storage/config"
	"dwalker/storage/repository/ent"
	"entgo.io/ent/dialect"
	entsql "entgo.io/ent/dialect/sql"
	_ "github.com/jackc/pgx/v4/stdlib"
	"github.com/rs/zerolog/log"
)

// NewClient returns new ent client from config values
func NewClient(cfg config.Config) *ent.Client {
	db, err := sql.Open("pgx", cfg.Database.Address)
	if err != nil {
		log.Err(err).Msg("failed describe database")
		panic(err)
	}

	// Create an ent.Driver from `db`.
	drv := entsql.OpenDB(dialect.Postgres, db)
	client := ent.NewClient(ent.Driver(drv))

	if err := client.Schema.Create(context.Background()); err != nil {
		log.Err(err).Msg("failed to create schema")
		panic(err)
	}
	return client
}
